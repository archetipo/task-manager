******
Manual
******

===========================================
Task Manager commands from the command line
===========================================

Another way to interact with the Task Manager is via a set of scripts/executables
available in the Task Manager environment (folders) after installing on a Windows
or Linux server. These executables are especially useful if you want to upload or
change a large number of tasks and/or templates. In that case, the REST API is
not very convenient since actions/commands are given one by one.

Available and useful commands that can be used from the Windows or Linux command
line box are shown below. Note that arguments between square brackets are optional.

--------------------
taskmanager-add-task
--------------------

Usage::

    taskmanager-add-task [-h] [-db] -p PROJECT [-u USERS [USERS ...]]
        [-g GROUPS [GROUPS ...]] [-t TAGS [TAGS ...]] [-d DISTRIBUTE_GROUP]
        [-gen GENERATOR_URL] [-an APPLICATION_NAME] [-av APPLICATION_VERSION]
        [-cu CALLBACK_URL] [-cc CALLBACK_CONTENT] [--url URL] [--old] task

Example::

    taskmanager-add-task -p glandula -u johndoe -t pinealis --url 10.20.30.40 --old task.json

By executing the example command above, a new task (JSON file task.json) is
uploaded to the Task Manager running on URL 10.20.30.40. HOSTNAME can be a IP
address or a server name known in the domain. The project related task is
"glandula" and the specific user the task is assigned to is "johndoe". The ``-t``
option is important here, since it means that this task, when carried out on
the ViewR will use template ‘pinealis’. So when uploading the task to the
Task Manager it is immediately linked to the task_template (ViewR template)
that has to be used whule running the task. The –old option here is used
because of a software version issue.

If the task would need to be assigned to a group of users (say with the name
phds), so anyone of them can pick the task up to carry out, you should give
the following::

    taskmanager-add-task -g phds -u johndoe -t pinealis --url 10.20.30.40 --old task.json

--------------------
taskmanager-add-user
--------------------

Usage::

    taskmanager-add-user [-h] -u USERNAME -p PASSWORD -n FULL_NAME -e EMAIL
        [-w ASSIGNMENT_WEIGHT] [-i] [-f]

This command adds a new user to the Task Manager. Most arguments will be
trivial/clear. The argument ``-h`` or ``--help`` will show the help message and
exit. The (optional) argument ``-I`` means that the new user should be
‘inactive’. The (optional) argument ``-f`` means that execution is forced
without asking questions.

------------------------
taskmanager-add-template
------------------------

Usage::

    taskmanager-add-template [-h] [--old] HOSTNAME template

Example::

    taskmanager-add -add --old http://10.20.30.40:5000 template.json

By executing the example command above, a new template (JSON file template.json)
will be uploaded to the Task Manager running on URL ``http://10.20.30.40`` on
port ``5000``.

---------------------------
Taskmanager-update-template
---------------------------

Usage::

    taskmanager-update-template [-h] [--name NAME] [--user USER] [--old]
        HOSTNAME TEMPLATE.JSON

For example::

    taskmanager-update-add --old http://10.20.30.40:5000 template.json

This command will update an existing template (known to and stored on the Task Manager).

===========
Permissions
===========

The permissions are defined as follows:

=================================   =================================================================================================================================
Permission                          Description
=================================   =================================================================================================================================
``task_read``                       Allows reading tasks assigned to itself. Please note: acquiring and releasing the lock field is part of this permission.
``task_read_all``                   Allows reading all tasks.
``task_update_status``              Allows updating the status of tasks assigned to itself.
``task_update_status_all``          Allows updating the status of all tasks.
``task_update_user``                Allows updating the assigned users and/or groups of tasks.
``task_update_lock_all``            Allows updating or clear the lock of all tasks.
``task_update_all``                 Allows updating all fields of all tasks.
``task_add``                        Allows adding a task.
``task_delete``                     Allows deleting a task.
``template_add``                    Allows adding a template.
``template_update``                 Allows updating templates.
``template_delete``                 Allows deleting a template.
``tag_add``                         Allows adding a tag.
``tag_update``                      Allows updating a tag.
``tag_delete``                      Allows deleting a tag.
``user_read``                       Allows seeing your user information.
``user_read_all``                   Allows seeing all users.
``user_add``                        Allows adding users.
``user_update_all``                 Allows updating all users.
``user_update_assignment_weight``   Allows changing the assignment weight of users.
``user_delete``                     Allows deleting users.
``group_read``                      Allows seeing the groups you are assigned to.
``group_read_all``                  Allows seeing all groups.
``group_add``                       Allows adding groups.
``group_update``                    Allows updating groups. Please note: this is how user membership of a group can be managed.
``group_delete``                    Allows deleting groups.
``roles_manage``                    Allows adding and removing roles from users
=================================   =================================================================================================================================


.. _Model:

===============
Database Models
===============

.. figure:: images/er_diagram_taskman.png
   :scale: 50%
   :alt: Database model for the Task Manager tables

   Database model of the Task Manager tables.

