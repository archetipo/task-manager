********
How To's
********

========================================
Developing and installing a task session
========================================

Let’s consider a relatively simple application, as an example to give more
insight in the workflow of the Task Manager. Suppose you want to assign the
task to a group of users to carry out a few measurements in an MR sequence
of the brain. And suppose that these measurements can be done by indicating a
set of anatomical landmarks in the MR data. The 3D coordinates of these
landmarks can then be used afterwards to calculate some anatomical distances
in the brain. Let’s consider that these measurements should be carried out in
a cohort of 1000 patients. This measurement application can be realized with
(for example) the following steps:

------------------------------------------------------
1. Add the group of users to the selected Taskmanager.
------------------------------------------------------

This means that the group should be defined in the Task Manager (if it is not
there already), the users should be defined (if they are not there already),
and these specific users should be added to (become a member of) the group
just created/defined.

Adding a new group and new users is done using the REST API.
To add a user, click on ``POST   /users``. You will see an example of the
input object::

    {
      "username": "string",
      "name": "string",
      "active": true,
      "email": "string",
      "password": "string",
      "assignment_weight": 0
    }

Click on “Try it out”. After that you can edit the six fields shown above. Fill
in the username, name (real name of the person, the email address, some
password, and the assignment_weight. The assignement weight is an indication of
how many tasks (of the total number available) will be shown to this user. If
the assignment_weight is 0, no task will be assigned to that specific user. If
the assignment_weight equals 1, all tasks will be assigned (shown) to that
user. If the user is a group member and the tasks are assigned to the group,
also other users can pick up the task. If the assignment_weight is for example
0.1, only 10 percent of the uploaded tasks (assigned to the group) will be
shown to this specific user. If a user is inactive (active == false), he or she
is currently not involved in carrying out tasks. After filling in all the
desired in a correct way, click on ‘execute’. Repeat this procedure for every
new user you want to add.

To define a new group, click on ‘POST group’. You will see the following example model::

    {
      "groupname": "string",
      "name": "string"
    }

Fill in (edit) the “groupname” (used as referring ‘id’) and also give a more
descriptive “name” in the second field. The click on execute. Now you are at
the fase of making the new users a member of the group. At the moment this is
not yet possible with the REST API. It should be done from ipython or by
executing python scripts directly on the database.

.. note::
    Adding a user to a group (make him/her a group member) is currently not
    yet possible with the REST API. For now it should done directly
    from the ipython command line or by python scripts.

--------------------------
2. Write the task_template
--------------------------

Writing a JSON file with the ‘description/definition’ of the Task Manager
task_template, needed to give the ViewR the optimal appearance in which
the desired measurements can be carried out.

Details on how to write a viewr task template can be found at
:ref:`ViewR template documentation <viewr:task_templates_reference>`.

-----------------------------------------------
3. Upload the task_template to the Task Manager
-----------------------------------------------

This means adding the template, mentioned/written in step 2 to the collection
of templates known by (stored in) the Task Manager. This can be done using the
REST API, but it is more convenient to use another approach, namely the
Task Manager command line scripts mentioned before. If you use the API you
have to click on ‘POST /task_templates’. You will see a model like this::

    {
      "label": "string",
      "content": {}
    }

You have to fill in the ``label``. That's just the name of the task_template
that will be used as a kind of referred name/id. The contents should be the
complete template (JSON) as shown in step 2. It is more convenient to put
this contents into a JSON file, and upload the file with command line scripts
(executables). For more details see the section
:ref:`Task Manager commands from the command line`.

---------------------------------
4. Write the specific task needed
---------------------------------

This means writing a JSON file with the overall ‘description/definition’ of
the task to be carried out by the users. This task has to be linked to the
desired template. In general a ‘task’ is less complex than a ‘template’ to
write. An important issue to pay attention to is that the task should be
linked to a template. This doesn’t mean only that the name of the task_template
that should be used for this task should be filled in to the model (JSON file),
but also that specific field in the task and task_template should be coherent
and consistently linked to each other. More details will follow using the next
example.

In this (example) case, the task could look this::

    {
        "scan_sources": {
            "T1":"http://10.20:30:40/data/experiments/E1234/scans/T1W/resources/NIFTI/files/T1W.nii.gz"
        },
        "_vars": {
            "SYSTEM_URL": "http://10.20:3:4",
            "EXPERIMENT_ID": "E1234",
            "LABEL": "L100"
        },
        "name": "L100 pinealis",
        "fields": {},
        "template": "glandula_pinealis",
        "fields_file": "http://10.20:3:4/data/experiments/E1234/resources/FIELDS/files/out{timestamp}.json",
        "windowing": {
        "width": 50.0,
        "center": 100.0
        }
    }

Important in this task example is the definition of the ``scan_sources``. In
this example only one MR sequence will be used and shown in the ViewR during
execution of the task. The MR sequence is a T1-weighted scan. The URL after
``T1``: points to the location on a specific XNAT host where the input data
(T1) for this task can be retrieved.

If you look at the template definition above, you will see that there is a
field ``scans``. Also there is only 1 sequence defined, a T1. The ``protocol``
and ``modality`` are additional info.

.. note::
    Keep in mind the name(s) of the input scan(s), in this case ``T1`` should be
    equal (consistent) in both the task.json and the template.json! If not the
    task will not run properly when loaded in the ViewR

---------------------------------------
5. Upload the tasks to the Task Manager
---------------------------------------

This means that for every dataset, specific MR sequence present in the cohort
of 1000 patients, a task should be defined and uploaded to the Task Manager.
This can almost be done using the REST API. With the REST API you have to post
all 1000 tasks (one measurement task per data set) one by one. It is therefore
more convenient to use the Task Manager command line scripts.

-------------------------
6. Apply the measurements
-------------------------

This means: start the ViewR (as one of the users) and connect it to the
Task Manager containing the info generated in step 1 to 5. This will be
described in more detail in the following section.

=====================
Items to be discussed
=====================

* Roles and privileges (at least user and admin)
* Description of the model ``Task``
* How to create a Task
* How to assign a Task to a user
* How to upload a Task to the Task Manager
* How to link a Task with a Template
* Status of a Task (queued, locked, done, etc)
* Relation ViewR, Task Manager, XNAT
* How to deal with Tasks in de ViewR
* TaskGroups, TaskTemplates, TaskTags
* Creating a group and/or adding a user to the groep is still difficult.
* Relation taskmodel with usermodel
* Usergroups
* Assignment weights
* Installation of the Task Manager? With a VirtualEnv? Or is that not needed?
* REST-API! ... Flask ... What can you post put get delete etc.
* How to connect the viewer to a specific Task Manager
* Callback to the Syncotron.
* With PUT you can change particular contents of tasks
* With status you can make a task ``done`` (REST API)
* Exploring the contents of the Taskmanager
* Roles! What can a user do ... and what can admin do ... and intermediate ... superuser