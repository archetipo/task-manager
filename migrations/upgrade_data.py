import argparse
import json

import requests

from taskmanager import create_app
from taskmanager import models

from flask_security import SQLAlchemyUserDatastore
from flask_security.utils import hash_password

db = models.db


def add_roles(username, password):
    print(f'Adding default roles')
    admin_role = models.Role(name='admin', description='Admin Role', permissions='task_read_all,task_update_all,task_add,task_delete,template_add,template_update,template_delete,tag_add,tag_update,tag_delete,user_read_all,user_update_all,user_add,user_delete,group_read_all,group_add,group_update,group_delete,roles_manage')
    db.session.add(admin_role)
    super_user_role = models.Role(name='superuser', description='Super User Role', permissions='task_read_all,task_update_status_all,task_update_user,task_update_lock_all,tag_add,tag_update,user_read_all,user_update_assignment_weight,group_read_all,group_update')
    db.session.add(super_user_role)
    user_role = models.Role(name='user', description='Normal User Role', permissions='task_read,task_update_status,user_read,group_read')
    db.session.add(user_role)
    db.session.commit()

    print(f'Adding default user roles')
    for user in models.User.query.all():
        user.active = True
        user.roles = [user_role]
    db.session.commit()

    print(f'Adding admin user {username}')
    user_datastore = SQLAlchemyUserDatastore(db, models.User, models.Role)
    user = user_datastore.create_user(
        username=username,
        name='Admin User',
        email='',
        password=hash_password(password),
    )
    db.session.add(user)
    db.session.commit()

    user.roles = [admin_role, user_role]
    db.session.commit()


def upgrade_tasks(study_governor_auth, new_hostname):
    session = requests.Session()
    session.auth = study_governor_auth

    for task in db.session.query(models.Task):
        content = json.loads(task.content)
        callback_url = task.callback_url

        # Make sure callback url is poiting to the test machine
        if isinstance(content, list):
            print(f'Callback url before: {callback_url}')

            if new_hostname:
                callback_url = callback_url.replace('10.191.20.122', new_hostname)

            if '/data/' in callback_url:
                    callback_url = callback_url.replace('/data/', '/api/v1/')

            print(f'Callback url after: {callback_url}')

            if callback_url and callback_url.endswith('/state'):

                # Figure out generator URL
                uri = callback_url.replace('/state', '/actions')
                response = session.get(uri)
                base, _, _, _, _, _ = callback_url.rsplit('/', 5)

                print(f'GOT URI: {uri}   RESPONSE: {response.status_code}')
                actions = response.json().get('actions', [])
                actions = reversed(actions)

                for action in actions:
                    action_uri = base + action
                    response = session.get(action_uri)
                    action_data = response.json()
                    action_response = action_data.get('return_value', '')

                    if not isinstance(action_response, str):
                        action_response = ''

                    if f'/tasks/{task.id}/lock' in action_response:
                        generator_url = action_uri
                        break
                else:
                    generator_url = callback_url.replace('/state', '')
            else:
                generator_url = ''

            print(f'[{task.id}] Found generator URL {generator_url}')

            if len(content) > 1:
                print(f'[{task.id}] Splitting task')

                # Create taskgroup and split task into parts
                subtasks = []
                for subcontent in content:
                    template = models.TaskTemplate.query.filter_by(label=subcontent['template']).one_or_none()
                    subtask = models.Task(
                        project=task.project,
                        template=template,
                        content=json.dumps(subcontent),
                        generator_url=generator_url,
                        application_name='ViewR',
                        application_version='5.1.5',
                    )
                    subtask.status = task.status
                    db.session.add(subtask)
                    subtasks.append(subtask)

                # Base new taskgroup label on the name
                task_label = subcontent['_vars']['LABEL']

                # Create taskgroup
                task_group = models.TaskGroup(
                   label=None,
                   callback_url=callback_url,
                   callback_content=task.callback_content,
                   tasks=subtasks,
                )
                db.session.add(task_group)

                # First remove groups and commit to avoid nast join error in the delete
                task.groups = []
                db.session.commit()

                # Do the actual delete, which should be safe now
                db.session.delete(task)
                db.session.commit()
            elif len(content) == 1:
                print(f'[{task.id}] Unpacking task')

                # Unpack tasks list
                task_label = content[0]['_vars']['LABEL']

                # Add new fields
                template = content[0]['template']
                template = models.TaskTemplate.query.filter_by(label=template).one_or_none()
                task.template = template
                task.content = json.dumps(content[0])
                task.generator_url = generator_url
                task.application_name = 'ViewR'
                task.application_version = '5.1.5'

                # Make sure to upgrade uri to new api
                task.callback_url = callback_url
                db.session.commit()
            else:
                print(f'[{task.id}] Not touching empty task')
        else:
            print(f'[{task.id}] Not touching non-list task')

    db.session.commit()


def upgrade(username, password, study_governor_auth, new_hostname):
    add_roles(username=username, password=password)
    upgrade_tasks(study_governor_auth=study_governor_auth, new_hostname=new_hostname)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Migrate data to handle role base access. This inserts the default setup.")
    parser.add_argument('--username', default='admin', help="Defines the username for the new admin user")
    parser.add_argument('--password', required=True, help="Defines the password for the new admin user")
    parser.add_argument('--study-user', required=True, help="The username to use to login the study governor")
    parser.add_argument('--study-password', required=True, help="The password to use to login the study governor")
    parser.add_argument('--new-hostname', required=False, help="New hostname to use in callback in case a change is desired")
    args = parser.parse_args()
    print(args)

    with create_app().app_context():
        upgrade(
            username=args.username,
            password=args.password,
            study_governor_auth=(args.study_user, args.study_password),
            new_hostname=args.new_hostname,
        )
