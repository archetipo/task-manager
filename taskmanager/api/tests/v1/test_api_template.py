# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import re

from flask import url_for
from flask_security.utils import hash_password

from taskmanager.tests.helpers import basic_auth_authorization_header

TIMESTAMP_REGEX = r'\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d'


def test_templates_get_as_admin(client, random_test_data):
    """ An admin user should be able to get a list of all templates. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.task_templates'), headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
  
  
def test_templates_get_as_superuser(client, random_test_data):
    """ An admin user should be able to get a list of all templates. """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.task_templates'), headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    print(data)


def test_templates_get_as_user(client, random_test_data):
    """ A user should be able to get a list of all templates. """
    #TODO do we really aloow this?
    headers = basic_auth_authorization_header('user2', 'user2')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.task_templates'), headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    print(data)


def test_template_get_as_admin(client, random_test_data):
    """ An admin user should be able to get a specific template. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    
    response = client.get(url_for('api_v1.task_template', id=3), headers=headers)
    assert response.status_code == 200
    data = response.get_json()
    print(data)


def test_template_get_as_superuser(client, random_test_data):
    """ A superuser should be able to get a specific template. """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    
    for x in range(1,4):
        response = client.get(url_for('api_v1.task_template', id=x), headers=headers)
        assert response.status_code == 200
        data = response.get_json()
        print(data)


def test_template_get_as_user(client, random_test_data):
    """ A user should be able to get a specific template. """
    #TODO Is that really what we wish?
    headers = basic_auth_authorization_header('user1', 'user1')
    headers.update({'accept': 'application/json'})
    
    for x in range(1,4):
        response = client.get(url_for('api_v1.task_template', id=x), headers=headers)
        assert response.status_code == 200
        data = response.get_json()
        print(data)


def test_template_get_nonexist_as_admin(client, random_test_data):
    """ An admin user should not be able to get a nonexisting template. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    
    response = client.get(url_for('api_v1.task_template', id=10000), headers=headers)
    assert response.status_code == 404
    data = response.get_json()
    print(data)


def test_template_get_nonexist_as_superuser(client, random_test_data):
    """ A superuser user should not be able to get a nonexisting template. """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    
    response = client.get(url_for('api_v1.task_template', id=10000), headers=headers)
    assert response.status_code == 404
    data = response.get_json()
    print(data)


def test_template_get_nonexist_as_user(client, random_test_data):
    """ A user user should not be able to get a nonexisting template. """
    headers = basic_auth_authorization_header('user2', 'user2')
    headers.update({'accept': 'application/json'})
    
    response = client.get(url_for('api_v1.task_template', id=10000), headers=headers)
    assert response.status_code == 404
    data = response.get_json()
    print(data)


def test_template_put_as_admin(client, random_test_data):
    """ An admin user should be able to put a template. """
    body = {
        "name": 'NewTemplate',
    }
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    
    response = client.put(url_for('api_v1.task_template', id=1), json=body, headers=headers)
    assert response.status_code == 200


def test_template_put_superuser(client, random_test_data):
    """ A superuser user should not be able to put a template. """
    body = {
        "name": 'NewTemplate',
    }
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    
    response = client.put(url_for('api_v1.task_template', id=1), json=body, headers=headers)
    assert response.status_code == 403


def test_template_put_as_user(client, random_test_data):
    """ A user user should not be able to put a template. """
    body = {
        "name": 'NewTemplate',
    }
    headers = basic_auth_authorization_header('user2', 'user2')
    headers.update({'accept': 'application/json'})
    
    response = client.put(url_for('api_v1.task_template', id=1), json=body, headers=headers)
    assert response.status_code == 403

    
def test_templates_post_as_admin(client, random_test_data):
    """ An admin user should be able to post templates """
    body = {
        "label": 'TemplateToBePosted',
        "content": 'AnyString',
    }
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    
    response = client.post(url_for('api_v1.task_templates'), json=body, headers=headers)
    assert response.status_code == 201


def test_templates_post_superuser(client, random_test_data):
    """ A superuser user should not be able to post templates. """
    body = {
        "name": 'TemplateToBePosted',
    }
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    
    response = client.post(url_for('api_v1.task_templates'), json=body, headers=headers)
    assert response.status_code == 403


def test_templates_post_as_user(client, random_test_data):
    """ A user user should not be able to post templates. """
    body = {
        "name": 'TemplateToBePosted',
    }
    headers = basic_auth_authorization_header('user2', 'user2')
    headers.update({'accept': 'application/json'})
    
    response = client.post(url_for('api_v1.task_templates'), json=body, headers=headers)
    assert response.status_code == 403


def test_template_delete_as_admin(client, random_test_data):
    #TODO
    #Internal ser error occurred ????
    """ An admin user should be able to delete a template. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    
    response = client.delete(url_for('api_v1.task_template', id=1), headers=headers)
    assert response.status_code == 200


def test_template_delete_superuser(client, random_test_data):
    """ A superuser should not be able to delete a template. """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    
    response = client.delete(url_for('api_v1.task_template', id=1), headers=headers)
    assert response.status_code == 403


def test_template_delete_as_user(client, random_test_data):
    """ A user should not be able to delete a template. """
    headers = basic_auth_authorization_header('user2', 'user2')
    headers.update({'accept': 'application/json'})
    
    response = client.delete(url_for('api_v1.task_template', id=3), headers=headers)
    assert response.status_code == 403


def test_template_delete_nonexisting_as_admin(client, random_test_data):
    """ An admin user should not be able to delete a nonexisting template. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    
    response = client.delete(url_for('api_v1.task_template', id=10000), headers=headers)
    assert response.status_code == 404