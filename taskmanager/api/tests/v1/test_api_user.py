# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import re

from flask import url_for
from flask_security.utils import hash_password

from taskmanager.tests.helpers import basic_auth_authorization_header

TIMESTAMP_REGEX = r'\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d'


def test_users_get_as_admin(client, app_config):
    """ An admin user should be able to get the list of users. """
    response = client.get(url_for('api_v1.users'), headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'users': [url_for('api_v1.user', id=x) for x in range(1, 8)]}


def test_users_get_as_superuser(client, app_config):
    """ A superuser should be able to get the list of users. """
    response = client.get(url_for('api_v1.users'), headers=basic_auth_authorization_header("superuser", "superuser"))
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'users': [url_for('api_v1.user', id=x) for x in range(1, 8)]}


def test_users_get_as_user(client, app_config):
    """ A normal user should NOT be able to get the list of users. """
    response = client.get(url_for('api_v1.users'), headers=basic_auth_authorization_header("user1", "user1"))
    assert response.status_code == 403
    #TODO: test if the body does not contain important information


def test_user_get_self_as_admin(client, app_config):
    """ An admin user should be able to retrieve it's own user information. """
    response = client.get(url_for('api_v1.user', id=1), headers=basic_auth_authorization_header('admin', 'admin'))
    assert response.status_code == 200

    data = response.get_json()

    # Check if create_time is in the response
    assert 'create_time' in data
    # get the create_time out if the response
    create_time = data.pop('create_time')

    # Check if time is a valid time format
    assert re.match(TIMESTAMP_REGEX, create_time) is not None

    # Test if everything is there expect for create_time
    assert data == {
        "username": "admin",
        "uri": "/api/v1/users/1",
        "name": "Admin",
        "active": True,
        "email": "admin@blaat.nl",
        "assignment_weight": 1.0,
        "tasks": "/api/v1/users/1/tasks",
        "groups": "/api/v1/users/1/groups"
      }


def test_user_get_other_as_admin(client, app_config):
    """ An admin should be able to get any user information. """
    response = client.get(url_for('api_v1.user', id=4), headers=basic_auth_authorization_header('admin', 'admin'))
    assert response.status_code == 200

    data = response.get_json()

    # Check if create_time is in the response
    assert 'create_time' in data
    # get the create_time out if the response
    create_time = data.pop('create_time')

    # Check if time is a valid time format
    assert re.match(TIMESTAMP_REGEX, create_time) is not None

    # Test if everything is there expect for create_time
    assert data == {
        "username": "user2",
        "uri": "/api/v1/users/4",
        "name": "User 2",
        "active": True,
        "email": "user2@blaat.nl",
        "assignment_weight": 1.0,
        "tasks": "/api/v1/users/4/tasks",
        "groups": "/api/v1/users/4/groups"
    }


def test_user_get_nonexisting_as_admin(client, app_config):
    """ An admin user should get a 404 (not found) when asking for a non existing user. """
    response = client.get(url_for('api_v1.user', id=10000), headers=basic_auth_authorization_header('admin', 'admin'))
    assert response.status_code == 404


def test_user_get_self_as_superuser(client, app_config):
    """ A superuser should be able to retrieve it's own user information. """
    response = client.get(url_for('api_v1.user', id=2), headers=basic_auth_authorization_header('superuser', 'superuser'))
    assert response.status_code == 200

    data = response.get_json()

    # Check if create_time is in the response
    assert 'create_time' in data
    # get the create_time out if the response
    create_time = data.pop('create_time')

    # Check if time is a valid time format
    assert re.match(TIMESTAMP_REGEX, create_time) is not None

    # Test if everything is there expect for create_time
    assert data == {
        "username": "superuser",
        "uri": "/api/v1/users/2",
        "name": "Super User",
        "active": True,
        "email": "superuser@blaat.nl",
        "assignment_weight": 1.0,
        "tasks": "/api/v1/users/2/tasks",
        "groups": "/api/v1/users/2/groups"
    }


def test_user_get_other_as_superuser(client, app_config):
    """ A superuser should be able to get any user information. """
    response = client.get(url_for('api_v1.user', id=5), headers=basic_auth_authorization_header('superuser', 'superuser'))
    assert response.status_code == 200

    data = response.get_json()

    # Check if create_time is in the response
    assert 'create_time' in data
    # get the create_time out if the response
    create_time = data.pop('create_time')

    # Check if time is a valid time format
    assert re.match(TIMESTAMP_REGEX, create_time) is not None

    # Test if everything is there expect for create_time
    assert data == {
        "username": "halfuser1",
        "uri": "/api/v1/users/5",
        "name": "Half User 1",
        "active": True,
        "email": "halfuser1@blaat.nl",
        "assignment_weight": 0.5,
        "tasks": "/api/v1/users/5/tasks",
        "groups": "/api/v1/users/5/groups"
    }


def test_user_get_nonexisting_as_superuser(client, app_config):
    """ A superuser should get a 404 (not found) when asking for a non existing user. """
    response = client.get(url_for('api_v1.user', id=10000), headers=basic_auth_authorization_header('superuser', 'superuser'))
    assert response.status_code == 404


def test_user_get_self_as_user(client, app_config):
    """ A normal user should be able to retrieve it's own user information. """
    response = client.get(url_for('api_v1.user', id=3), headers=basic_auth_authorization_header('user1', 'user1'))
    assert response.status_code == 200

    data = response.get_json()

    # Check if create_time is in the response
    assert 'create_time' in data
    # get the create_time out if the response
    create_time = data.pop('create_time')

    # Check if time is a valid time format
    assert re.match(TIMESTAMP_REGEX, create_time) is not None

    # Test if everything is there expect for create_time
    assert data == {
        "username": "user1",
        "uri": "/api/v1/users/3",
        "name": "User 1",
        "active": True,
        "email": "user1@blaat.nl",
        "assignment_weight": 1.0,
        "tasks": "/api/v1/users/3/tasks",
        "groups": "/api/v1/users/3/groups"
    }


def test_user_get_other_as_user(client, app_config):
    """ A normal user should not be able to get information from another user. """
    response = client.get(url_for('api_v1.user', id=5), headers=basic_auth_authorization_header('user1', 'user1'))
    assert response.status_code == 403


def test_users_get_not_authenticated(client, app_config, config):
    """ An anonymous user should be getting a 401 Unauthorized and a WWW-Authenticate header in the response. """
    response = client.get(url_for('api_v1.users'), headers={'accept': 'application/json'})
    assert response.status_code == 401

    assert 'WWW-Authenticate' in response.headers
    assert f'Basic realm="{config["SECURITY_DEFAULT_HTTP_AUTH_REALM"]}"' == response.headers['WWW-Authenticate']


def test_user_get_not_authenticated(client, app_config, config):
    """ An anonymous user should be getting a 401 Unauthorized and a WWW-Authenticate header in the response. """
    response = client.get(url_for('api_v1.user', id=1), headers={'accept': 'application/json'})
    assert response.status_code == 401

    assert 'WWW-Authenticate' in response.headers
    # TODO: get the realm from the security config
    assert f'Basic realm="{config["SECURITY_DEFAULT_HTTP_AUTH_REALM"]}"' == response.headers['WWW-Authenticate']


def test_users_post_as_admin(client, app_config):
    """ Adding a user as an admin should be allowed. """
    body = {
        "username": "test_user",
        "name": "Test User",
        "email": "test@email.com",
        "password": "KR9n&6$E70Z!hLEi",
        "assignment_weight": 0.8
    }
    response = client.post(url_for('api_v1.users'),
                           json=body,
                           headers=basic_auth_authorization_header('admin', 'admin'))
    assert response.status_code == 201

    data = response.get_json()
    # Checks if all data is correct
    assert data['username'] == body['username']
    assert data['name'] == body['name']
    assert data['email'] == body['email']
    assert re.match(TIMESTAMP_REGEX, data['create_time']) is not None
    assert data['assignment_weight'] == body['assignment_weight']

    # Check if the uri's respond with 200 on a GET request.
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    uri_response = client.get(data['uri'], headers=headers)
    assert uri_response.status_code == 200
    tasks_response = client.get(data['tasks'], headers=headers)
    assert tasks_response.status_code == 200
    groups_response = client.get(data['groups'], headers=headers)
    assert groups_response.status_code == 200

    # Check if the password is not given back.
    assert 'password' not in data


def test_users_post_as_superuser(client, app_config, config):
    """ Adding a user as an superuser should NOT be allowed. """
    body = {
        "username": "test_user",
        "name": "Test User",
        "email": "test@email.com",
        "password": "KR9n&6$E70Z!hLEi",
        "assignment_weight": 0.8
    }
    response = client.post(url_for('api_v1.users'),
                           json=body,
                           headers=basic_auth_authorization_header('superuser', 'superuser'))
    assert response.status_code == 403

    data = response.get_json()
    assert data["response"]["error"] == config['SECURITY_MSG_UNAUTHORIZED'][0]


def test_users_post_as_user(client, app_config, config):
    """ Adding a user as an user should NOT be allowed. """
    body = {
        "username": "test_user",
        "name": "Test User",
        "email": "test@email.com",
        "password": "KR9n&6$E70Z!hLEi",
        "assignment_weight": 0.8
    }
    response = client.post(url_for('api_v1.users'),
                           json=body,
                           headers=basic_auth_authorization_header('user1', 'user1'))
    assert response.status_code == 403

    data = response.get_json()
    assert data["response"]["error"] == config['SECURITY_MSG_UNAUTHORIZED'][0]


def test_users_post_as_anonymous(client, app_config, config):
    """ Adding a user as an user should NOT be allowed. """
    body = {
        "username": "test_user",
        "name": "Test User",
        "email": "test@email.com",
        "password": "KR9n&6$E70Z!hLEi",
        "assignment_weight": 0.8
    }
    response = client.post(url_for('api_v1.users'), json=body, headers={'accept': 'application/json'})
    assert response.status_code == 401

    data = response.get_json()
    assert data["response"]["error"] == config['SECURITY_MSG_UNAUTHENTICATED'][0]


def test_users_post_as_admin_missing_required_argument(client, app_config):
    """ One of the required arguments is missing, this should return something went wrong [400]. """
    body = {
        "name": "Test User",
        "email": "test@email.com",
        "password": "KR9n&6$E70Z!hLEi",
        "assignment_weight": 0.8
    }
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.post(url_for('api_v1.users'), json=body, headers=headers)
    assert response.status_code == 400

    data = response.get_json()
    print(data)
    # Check if the error message is about the required argument that was malformed.
    assert "Missing required parameter" in data["errors"]["username"]


def test_users_post_as_admin_missing_optional_argument(client, app_config):
    """ One of the optional arguments is missing, this should create the user as expected with the default value for the missing parameter. """
    body = {
        "username": "test_user1",
        "name": "Test User1",
        "email": "test1@email.com",
        "password": "KR9n&6$E70Z!hLEi"
    }
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.post(url_for('api_v1.users'), json=body, headers=headers)
    assert response.status_code == 201

    data = response.get_json()
    print(data)
    # Check if the assignment_weight has gotten the correct default value.
    assert data['assignment_weight'] == 1.0


def test_user_put_as_admin(client, app_config):
    """ Test if the admin can put a different name on the users endpoint. This should be allowed. """
    body = {
        "name": "Changed Name",
    }
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.put(url_for('api_v1.user', id=3), json=body, headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    # Check if the data is correct in the response.
    assert data["name"] == body["name"]


def test_user_put_more_as_admin(client, app_config):
    """ Test if the admin can put more fields on the users endpoint. This should be allowed. """
    body = {
        "password": hash_password('blaat123'),
        "username": "other_user_name",
        "active": True,
        "email": "some@email.org",
        "assignment_weight": 0.6
    }
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.put(url_for('api_v1.user', id=5), json=body, headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    # Check if the data is correct in the response.
    body.pop('password')
    for key in body.keys():
        assert data[key] == body[key]


def test_user_put_as_admin_non_existing_user(client, app_config):
    """ Test if the admin can put a different name on the users endpoint of a non existing user. This should fail. """
    body = {
        "name": "Changed Name",
    }
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.put(url_for('api_v1.user', id=10), json=body, headers=headers)
    assert response.status_code == 404


def test_user_put_as_superuser_fields(client, app_config):
    """ Test if the superuser can put different fields on the users endpoint. This should NOT be allowed. """
    fields = [
        ('username', 'some_other_name', 'You are not authorized to change the username'),
        ('name', 'Changed Name', 'You are not authorized to change the name'),
        ('password', 'new_password_hash', 'You are not authorized to change the password'),
        ('active', True, 'You are not authorized to change the activeness'),
        ('email', 'some.other@email.com', 'You are not authorized to change the email')
    ]
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    for fieldname, value, error_message in fields:
        print(f"{fieldname}: {value}")
        response = client.put(url_for('api_v1.user', id=3), json={fieldname: value}, headers=headers)
        assert response.status_code == 403

        data = response.get_json()
        # Check if the error message corresponds to what we expect
        assert data["message"] == error_message


def test_user_put_as_superuser_assignment_weight(client, app_config):
    """ Test if the superuser can put a different assignment_weight on the users endpoint. This should be allowed. """
    body = {
        "assignment_weight": 0.3,
    }
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    response = client.put(url_for('api_v1.user', id=3), json=body, headers=headers)
    print(response.get_json())
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    # Check if the data in the response is what we expect.
    assert data["assignment_weight"] == body["assignment_weight"]


def test_user_put_as_user_fields(client, app_config, config):
    """ Test if a user can put a different fields on the users endpoint. This should NOT be allowed. """
    fields = [
        ('username', 'some_other_name'),
        ('name', 'Changed Name'),
        ('password', 'new_password_hash'),
        ('active', True),
        ('email', 'some.other@email.com')
    ]
    headers = basic_auth_authorization_header('user2', 'user2')
    headers.update({'accept': 'application/json'})
    for fieldname, value in fields:
        response = client.put(url_for('api_v1.user', id=3), json={fieldname: value}, headers=headers)
        assert response.status_code == 403

        data = response.get_json()
        # Check if the error message corresponds to what we expect.
        assert data["response"]["error"] == config['SECURITY_MSG_UNAUTHORIZED'][0]


def test_user_put_as_user_assignment_weight(client, app_config, config):
    """ Test if a user can put a different assignment_weight on the users endpoint. This should NOT be allowed. """
    body = {
        "assignment_weight": 0.3
    }
    headers = basic_auth_authorization_header('user2', 'user2')
    headers.update({'accept': 'application/json'})
    response = client.put(url_for('api_v1.user', id=3), json=body, headers=headers)
    assert response.status_code == 403

    data = response.get_json()
    # Check if the error message corresponds to what we expect.
    assert data["response"]["error"] == config['SECURITY_MSG_UNAUTHORIZED'][0]


def test_user_put_as_anonymous_name(client, app_config, config):
    """ Test if a anonymous user can put a different name on the users endpoint. This should NOT be allowed. """
    body = {
        "name": "Changed Name",
    }
    headers = {'accept': 'application/json'}
    response = client.put(url_for('api_v1.user', id=3), json=body, headers=headers)
    print(response.get_json())
    assert response.status_code == 401

    data = response.get_json()
    print(data)
    # Check if the error message corresponds to what we expect.
    assert data["response"]["error"] == config['SECURITY_MSG_UNAUTHENTICATED'][0]


def test_user_inactive_on_config(client, app_config):
    """ Tests if a user that should be inactive from the configuration is indeed inactive. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.user', id=7), headers=headers)
    data = response.get_json()
    assert 'name' in data
    assert data['username'] == 'inactiveuser'
    assert 'active' in data
    assert data['active'] == False


def test_user_active_on_config(client, app_config):
    """ Tests if a user that be active from the configuration is indeed active. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.user', id=6), headers=headers)
    data = response.get_json()
    assert 'name' in data
    assert data['username'] == 'halfuser2'
    assert 'active' in data
    assert data['active'] == True


def test_user_delete_as_admin(client, app_config):
    """ Tests if an admin can delete a user. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.delete(url_for('api_v1.user', id=6), headers=headers)
    assert response.status_code == 200

    # Check if the user is set inactive.
    response = client.get(url_for('api_v1.user', id=6), headers=headers)
    data = response.get_json()
    assert data['active'] == False


def test_user_delete_as_superuser(client, app_config):
    """ Tests if a superuser can delete a user. This should NOT be possible! """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    response = client.delete(url_for('api_v1.user', id=6), headers=headers)
    assert response.status_code == 403


def test_user_delete_as_user(client, app_config):
    """ Tests if a normal user can delete a user. That should NOT be possible! """
    headers = basic_auth_authorization_header('user1', 'user1')
    headers.update({'accept': 'application/json'})
    response = client.delete(url_for('api_v1.user', id=6), headers=headers)
    assert response.status_code == 403


def test_user_delete_as_anonymous(client, app_config):
    """ Tests if an anonymous user can delete a user. That should NOT be possible! """
    headers = {'accept': 'application/json'}
    response = client.delete(url_for('api_v1.user', id=6), headers=headers)
    assert response.status_code == 401


def test_user_delete_non_existing(client, app_config):
    """ Tests if a delete on a non existing user returns a 404. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.delete(url_for('api_v1.user', id=50), headers=headers)
    assert response.status_code == 404


def test_user_get_groups_as_admin(client, app_config):
    """ Tests if the admin can get the groups a certain user belongs to. Should be forbidden (403). """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.usergroups', id=3), headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'groups': [url_for('api_v1.group', id=x) for x in range(1, 3)]}


def test_user_get_groups_as_admin_nonexisting(client, app_config):
    """ Tests if the admin can get the groups a certain user belongs to. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.usergroups', id=50), headers=headers)
    assert response.status_code == 404


def test_user_get_groups_as_superuser(client, app_config):
    """ Tests if a superuser can get the groups a certain user belongs to. """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.usergroups', id=3), headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'groups': [url_for('api_v1.group', id=x) for x in range(1, 3)]}


def test_user_get_groups_as_user_from_self(client, app_config):
    """ Tests if a user can get the groups a it belongs to. """
    headers = basic_auth_authorization_header('user1', 'user1')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.usergroups', id=3), headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'groups': [url_for('api_v1.group', id=x) for x in range(1, 3)]}


def test_user_get_groups_as_user_from_other(client, app_config):
    """ Tests if a user can get the groups another user belongs to. """
    headers = basic_auth_authorization_header('user2', 'user2')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.usergroups', id=3), headers=headers)
    assert response.status_code == 403


def test_user_get_groups_as_anonymous(client, app_config):
    """ Tests if a an anonymous user can get the groups of an existing user. Should get a unauthenticated (401). """
    headers = {'accept': 'application/json'}
    response = client.get(url_for('api_v1.usergroups', id=3), headers=headers)
    assert response.status_code == 401


def test_user_get_tasks_as_admin(client, random_test_data):
    """ Tests if the admin can get the tasks a certain user belongs to. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.usertasks', id=3), headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list)
    assert 'status' in data['tasks'][0]


def test_user_get_tasks_as_admin_nonexisting(client, random_test_data):
    """ Tests if the admin can get the tasks of a non-existing user. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.usertasks', id=50), headers=headers)
    assert response.status_code == 404


def test_user_get_tasks_as_superuser(client, random_test_data):
    """ Tests if the superuser can get the tasks a certain user belongs to. """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.usertasks', id=3), headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list)
    assert 'status' in data['tasks'][0]


def test_user_get_tasks_as_user_self(client, random_test_data):
    """ Tests if a user can get its own tasks. """
    headers = basic_auth_authorization_header('user1', 'user1')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.usertasks', id=3), headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list)
    assert 'status' in data['tasks'][0]


def test_user_get_tasks_as_user_other(client, app_config):
    """ Tests if a user can get the tasks of a certain other user. Should be forbidden (403). """
    headers = basic_auth_authorization_header('user2', 'user2')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.usertasks', id=3), headers=headers)
    assert response.status_code == 403


def test_user_get_tasks_as_anonymous(client, app_config):
    """ Tests if a an anonymous user can get the tasks of an existing user. Should get a unauthenticated (401). """
    headers = {'accept': 'application/json'}
    response = client.get(url_for('api_v1.usertasks', id=3), headers=headers)
    assert response.status_code == 401


def test_user_put_new_role_as_admin(client, app_config):
    """ Tests if an admin can change the role of a user. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    # Try to add the superuser role to user1
    user_id = 3
    role_id = 2
    response = client.put(url_for('api_v1.userrole', user_id=user_id, role_id=role_id), headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    assert data['user'] == user_id
    assert data['role'] == role_id
    assert data['has_role'] == True


def test_user_put_existing_role_as_admin(client, app_config):
    """ Tests if an admin can change the role of a user. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    # Try to add the user role to user1
    user_id = 3
    role_id = 3
    response = client.put(url_for('api_v1.userrole', user_id=user_id, role_id=role_id), headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    assert data['user'] == user_id
    assert data['role'] == role_id
    assert data['has_role'] == True


def test_user_put_role_as_admin_on_username_role_id(client, app_config):
    """ Tests if an admin can change the role of a user using username and role_id. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    # Try to add the superuser role to user1
    user_id = "user1"
    role_id = 2
    response = client.put(url_for('api_v1.userrole', user_id=user_id, role_id=role_id), headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    # The api gives always the id back, so test on user id of 'user1' = 3
    assert data['user'] == 3
    assert data['role'] == role_id
    assert data['has_role'] == True


def test_user_put_role_as_admin_on_user_id_rolename(client, app_config):
    """ Tests if an admin can change the role of a user using user_id and rolename. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    # Try to add the superuser role to user1
    user_id = 3
    role_id = "superuser"
    response = client.put(url_for('api_v1.userrole', user_id=user_id, role_id=role_id), headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    # The api gives always the id back, so test on role id of 'superuser' = 2
    assert data['user'] == user_id
    assert data['role'] == 2
    assert data['has_role'] == True


def test_user_put_nonexisting_role_as_admin(client, app_config):
    """ Tests if an admin can change the role of a user. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    # Try to add a non existing role to user1
    user_id = 3
    role_id = 10
    response = client.put(url_for('api_v1.userrole', user_id=user_id, role_id=role_id), headers=headers)
    assert response.status_code == 404

    data = response.get_json()
    assert "Could not find Role" in data['message']


def test_user_put_nonexisting_user_as_admin(client, app_config):
    """ Tests if an admin can change the role of a user. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    # Try to add the superuser role to a non existing user
    user_id = 100
    role_id = 2
    response = client.put(url_for('api_v1.userrole', user_id=user_id, role_id=role_id), headers=headers)
    assert response.status_code == 404

    data = response.get_json()
    assert "Could not find User" in data['message']


def test_user_put_new_role_as_superuser(client, app_config):
    """ Tests if a superuser can change the role of a user. """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    # Try to add the superuser role to user1
    user_id = 3
    role_id = 2
    response = client.put(url_for('api_v1.userrole', user_id=user_id, role_id=role_id), headers=headers)
    assert response.status_code == 403


def test_user_put_existing_role_as_superuser(client, app_config):
    """ Tests if an admin can change the role of a user. """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    # Try to add the user role to user1
    user_id = 3
    role_id = 3
    response = client.put(url_for('api_v1.userrole', user_id=user_id, role_id=role_id), headers=headers)
    assert response.status_code == 403


def test_user_put_existing_role_as_anonymous(client, app_config):
    """ Tests if an admin can change the role of a user. """
    headers = {'accept': 'application/json'}
    # Try to add the superuser role to user1
    user_id = 3
    role_id = 2
    response = client.put(url_for('api_v1.userrole', user_id=user_id, role_id=role_id), headers=headers)
    assert response.status_code == 401


def test_user_delete_role_from_user_as_admin(client, app_config):
    """ Tests if an admin can delete a role from a user. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    user_id = 4
    role_id = 3
    response = client.delete(url_for('api_v1.userrole', user_id=user_id, role_id=role_id), headers=headers)
    assert response.status_code == 200
    data = response.get_json()
    assert data == {'role': role_id, 'user': user_id, 'has_role': False}


def test_user_delete_nonexisting_role_from_user_as_admin(client, app_config):
    """ Tests if an admin can delete a role from a user. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    user_id = 4
    role_id = 100
    response = client.delete(url_for('api_v1.userrole', user_id=user_id, role_id=role_id), headers=headers)
    assert response.status_code == 404
    data = response.get_json()
    assert f"Could not find Role with identifier {role_id}" in data['message']


def test_user_delete_role_from_nonexisting_user_as_admin(client, app_config):
    """ Tests if an admin can delete a role from a user. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    user_id = 100
    role_id = 3
    response = client.delete(url_for('api_v1.userrole', user_id=user_id, role_id=role_id), headers=headers)
    assert response.status_code == 404
    data = response.get_json()
    assert f"Could not find User with identifier {user_id}" in data['message']


def test_user_delete_role_from_user_as_superuser(client, app_config):
    """ Tests if a superuser can delete a role from a user. """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    user_id = 4
    role_id = 3
    response = client.delete(url_for('api_v1.userrole', user_id=user_id, role_id=role_id), headers=headers)
    assert response.status_code == 403


def test_user_delete_role_from_user_as_user(client, app_config):
    """ Tests if a user can delete a role from a user. """
    headers = basic_auth_authorization_header('user1', 'user1')
    headers.update({'accept': 'application/json'})
    user_id = 4
    role_id = 3
    response = client.delete(url_for('api_v1.userrole', user_id=user_id, role_id=role_id), headers=headers)
    assert response.status_code == 403