# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import re

from flask import url_for
from flask_security.utils import hash_password

from taskmanager.tests.helpers import basic_auth_authorization_header

TIMESTAMP_REGEX = r'\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d'


def test_taskgroups_get_as_admin(client, random_test_data):
    """ An admin user should be able to get the taskgroups. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.taskgroups', id=1), headers=headers)
    assert response.status_code == 200

#data = response.get_json()
#assert 'tasks' in data
#assert isinstance(data['tasks'], list)
#assert 'status' in data['tasks'][4]
#assert 'tags' in data['tasks'][4]
#print(data['tasks'][4])
  
  
def test_taskgroup_get_as_superuser(client, random_test_data):
    """ An admin user should be able to get the tags from a task. """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.taskgroups', id=2), headers=headers)
    assert response.status_code == 200


def test_taskgroup_get_as_user(client, random_test_data):
    """ A user should be able to get the tags from a task assigned to him/her. """
    headers = basic_auth_authorization_header('user1', 'user1')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.taskgroups', id=3), headers=headers)
    assert response.status_code == 200


def test_taskgroups_get_all_as_admin(client, random_test_data):
    """ An admin user should be able to get an overview of all. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    
    for x in range(1,4):
        response = client.get(url_for('api_v1.taskgroup', id=x), headers=headers)
        assert response.status_code == 200
        data = response.get_json()
        print(data)


def test_taskgroups_get_all_as_superuser(client, random_test_data):
    """ A superuser should be able to get an overview of all. """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    
    for x in range(1,4):
        response = client.get(url_for('api_v1.taskgroup', id=x), headers=headers)
        assert response.status_code == 200
        data = response.get_json()
        print(data)


def test_taskgroups_get_all_as_user(client, random_test_data):
    """ A user should be able to get an overview of all. """
    #TODO Is that really what we wish?
    headers = basic_auth_authorization_header('user1', 'user1')
    headers.update({'accept': 'application/json'})
    
    for x in range(1,4):
        response = client.get(url_for('api_v1.taskgroup', id=x), headers=headers)
        print(response.get_json())
        assert response.status_code == 200

"""
{
  "label": "string",
  "callback_url": "string",
  "callback_content": "string",
  "distribute_in_group": "string",
  "distribute_method": "string",
  "tasks": [
    {
      "project": "string",
      "template": "string",
      "content": "string",
      "tags": [
        "string"
      ],
      "callback_url": "string",
      "callback_content": "string",
      "generator_url": "string",
      "application_name": "string",
      "application_version": "string",
      "users": [
        "string"
      ],
      "groups": [
        "string"
      ],
      "distribute_in_group": "string"
    }
  ]
}
"""
def test_taskgroup_post_new_taskgroup_as_admin(client, random_test_data):
    """ An admin should be able to post a new taskgroup. """
    task_info = {
        "content": "Task content",
        "project": "newproject",
        "callback_url": "https://url/to/endpoint",
        "callback_content": "some cool stuff",
        "template": "microbleeds",
        "generator_url": "https://this/test",
    }

    body = {
        "label": "TaskGroupCreatedByAdmin",
        "callback_url": "http://localhost:5000/taskgroup/1",
        "callback_content": "{\"function\": \"do_something_useful\"}",
        "distribute_in_group": "inspectors",
        "tasks": [task_info, task_info, task_info]
    }

    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    
    response = client.post(url_for('api_v1.taskgroups'), json=body, headers=headers)
    print(response.get_json())
    assert response.status_code == 201

    for x in range(1,5):
        response = client.get(url_for('api_v1.taskgroups', id=x), headers=headers)
        assert response.status_code == 200
        data = response.get_json()
        print(data)


def test_taskgroup_put_as_admin(client, random_test_data):
    """ Test if the admin can put a tag to a specific task. This should be allowed. """
    body = {
        "label": "TaskGroup 111",
    }
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.put(url_for('api_v1.taskgroup', id=1), json=body, headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    # Check if the data is correct in the response.
    assert data["label"] == body["label"]
    #TODO are there more datafields that can be set in group?


def test_taskgroup_put_as_superuser(client, random_test_data):
    """ Test if the admin can put a tag to a specific task. This should be allowed. """
    body = {
        "label": "TaskGroup 111",
        "status": "queued",
    }
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    response = client.put(url_for('api_v1.taskgroup', id=1), json=body, headers=headers)
    assert response.status_code == 403



def test_taskgroup_put_as_user(client, random_test_data):
    """ Test if the admin can put a tag to a specific task. This should be allowed. """
    body = {
        "label": "TaskGroup 111",
        "status": "queued",
    }
    headers = basic_auth_authorization_header('user2', 'user2')
    headers.update({'accept': 'application/json'})
    response = client.put(url_for('api_v1.taskgroup', id=1), json=body, headers=headers)
    assert response.status_code == 403


def test_taskgroup_put_nonexisting_as_admin(client, random_test_data):

    """ Test if the admin can put a tag to a specific task. This should be allowed. """
    body = {
        "label": "TaskGroup 999",
        "status": "queued",
    }
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.put(url_for('api_v1.taskgroup', id=10000), json=body, headers=headers)
    assert response.status_code == 404





