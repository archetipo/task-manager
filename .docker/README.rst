Using docker for the task-manager
=================================

Building the docker container manually
--------------------------------------

When in the root of the repositoy run the following command to build 
the container:

::

   docker build --pull -t taskmanager:latest -f .docker/Dockerfile .

You need to build the container from the root of the repository because 
the Dockerfile needs access to files outside of the `.docker` directory.


Pushing the docker-contianer to the registry
--------------------------------------------

To push the container under a specific tag to the registry run the 
following commands, in this case the tag `testing` is given:

::

   CURI=registry.gitlab.com/radiology/infrastructure/task-manager
   docker login $CURI
   docker tag taskmanager:latest $CURI:testing
   docker push $CURI:testing


You can replace $CURI with something you are allowed to write to.



Running with docker-compose
---------------------------

Just run:

::

   docker-compose up -d && docker-compose logs -f

This will give you a test instance of the taskmanager.
